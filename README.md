# Conky_script

Conky script to get time from different time zones + info about system (RAM, CPU, process, temperatures, networking, etc...).
The conky.conf file to edit should be in etc/conky/ after you install it (sudo apt-get install conky-all)
I adapted it to my needs, edit it your way, it's quite intuitive, more info: https://www.linux.com/tutorials/how-install-and-configure-conky/

Looks like this:

![Conkypic](conky_window.png)